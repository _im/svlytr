Card = require("./card").Card
sharp = require "sharp"
fs = require "fs"

jpg_quality = 70
background_color = "#00000000"
# background_color = "#ff00ff"

module.exports =
  create_card_atlas : (cards, init) ->
    W = Card.atlas_width()
    # w = Math.floor(W / 8)
    w = Card.size.small
    image = sharp(
      create : 
        width : W
        height : W
        channels : 4
        background : r : 0, g : 0, b : 255, alpha : 0
    )
    image = image
      .composite(
        cards.map (c, i) ->
          p = c.get_atlas_pos(c.id)
          {
            input : "./images/#{c.file}.#{Card.format}"
            left : p.x
            top : W - p.y
          }

       )

    image
      .png()
      .toFile("./images/atlas.png")
      .then((d) ->
        init()
      )
    image
      .jpeg(
        quality : jpg_quality
      )
      .toFile("./images/atlas.jpg")

  create_image_maps : (cards, init) ->
    processed = 0
    cards.map (c, i) ->
      path = "./raw/#{c.file}_a.#{Card.format}"
      console.log path
      image = sharp path

      op_per_card = 3

      finish = () =>
        
        # console.log cards
        module.exports.create_card_atlas cards, init

      progress_gen = (_c, s) ->
        processed++
        console.log "[#{processed}/#{cards.length * op_per_card}] /images/#{_c.file}.#{Card.format}" + " -> #{s}"
        if processed is cards.length * op_per_card
          finish()

      generate_ab = (d) ->
        cards[i].set_size d, 0
        w = Math.floor(Card.size.large / 2)

        progress_gen c, "A"

        if d.width > d.height
          image = image.rotate(90)

        console.log background_color
        image
          .resize(w)
          .resize(w, Card.size.large, {
            fit : sharp.fit.contain, 
            position : sharp.position.bottom
            background : background_color
          })
          .png()
          .toFile("images/#{c.file}_a.png")
          .then (a_side) -> 
            # console.log "[#{j}/#{cs.length}] saving images/#{c}"
            progress_gen c, "B"
            b_side = sharp "./raw/#{c.file}_b.#{Card.format}"

            b_side
              .metadata()
              .then (b) ->

                cards[i].set_size b, 0

                b_side
                  .resize(w)
                  .resize(w, Card.size.large, {
                    fit : sharp.fit.contain
                    position : sharp.position.bottom
                    background : background_color
                  })
                  .extend({
                    top: 0,
                    bottom: 0,
                    left: w,
                    right: 0,
                    background : background_color
                  })
                  .composite([
                    input : "images/#{c.file}_a.png"                  
                    gravity : "southwest"
                  ])
                  .toFile("images/#{c.file}.png")
                  .then (d) ->
                    progress_gen c, "done."

      image
        .metadata()
        .then generate_ab