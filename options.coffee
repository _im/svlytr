module.exports =
  version : "0.2"
  resolution : 
    "640x480"
  resolutions : [
      "160x120"
      "320x240"
      "640x480"
      "1024x768"
      "1280x720"
    ]
  screen :
    w : 640
    h : 480
  oversample : 3
  closed_help : false
  show_camera : true
  speed : 0.1
  line_color : "#fa2"
  refresh_cycle : 16
  font_size : 38

  detector : "saturator"

  plane_width : window.innerWidth
  plane_height : window.innerHeight

  flip_x : true
  flip_y : false
  
  corner_names : "ABCD"
  help : ""
  error_messages : 
    header : 
      """
      # **!!! Apocalyptic Error !!!**


      """
    webmidi : 
      """
      maybe your browser doesn't support midi? 
      - try another browser (tested with chromium)
      """
    midi : 
      """
      no usable midi output found!
      
      you need a midi port where this program can send the CC messages

        - windows - see [loopMIDI](https://www.tobias-erichsen.de/software/loopmidi.html)
        - osx - try [this](https://support.apple.com/guide/audio-midi-setup/transfer-midi-information-between-apps-ams1013/mac)
        - linux - try [this](http://tldp.org/HOWTO/MIDI-HOWTO-10.html)
      """
    webcam_permission : 
      """
      you have to give permission to your webcamera to use this site!
      """
    webcam : 
      """
      no webcamera was found.
      """