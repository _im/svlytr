varying vec2 vUv;
uniform sampler2D tex0;

uniform vec3 white;


vec3 rgb2hsb(vec3 c){
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = c.g < c.b ? vec4(c.bg, K.wz) : vec4(c.gb, K.xy);
    vec4 q = c.r < p.x ? vec4(p.xyw, c.r) : vec4(c.r, p.yzx);

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsb2rgb(vec3 c){
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main() {

  vec2 uv = vUv;

  vec4 tex = texture2D(tex0, uv);

  vec3 hsb = rgb2hsb(tex.rgb);

  // vec3 w = rgb2hsb(white);

  // hsb.y -= abs(hsb.x - w.x);


  // hsb.r += w.x * w.y * w.b;
  // hsb.r = fract(hsb.r);


  // if (hsb.y > 0.7){
  //   hsb.y = 1.0;
  //   hsb.z = 1.0;
  //   gl_FragColor = vec4(hsb2rgb(hsb), 1.0);
  // }else{
  //   // gl_FragColor = vec4(hsb2rgb(hsb), 1.0);
  //   gl_FragColor = vec4(0,0,0, 1.0);
  // }


  // vec4 c = texture2D( tex0, vUv );
  // float s = c.r + c.g + c.b;
  // s = s > 0.3 ? 1.0 : 0.0;
  // gl_FragColor = vec4(s, s, s, 1);

  // gl_FragColor = vec4(vec3(hsb.r), 1);
  // gl_FragColor = vec4(tex.rg, 1, 1);
  gl_FragColor = tex;
}

// void main() {

// }
