_ = (c) ->
  console.log c
  c
  
window.already_connected = false

if io?
  window.socket = io()
  _ window.socket
  _ "started mupr"

  socket.on "connect", () -> 
    if window.already_connected
      _ "reloading..."
      window.location.reload()
    else
      window.already_connected = true
      _ "connected"

Card = require("./card").Card
window.cards = require("./images/cards.json").map((c) -> Card.load(c))
window.cards.sort((a, b) -> 1 - Math.random() * 2)

window.T = require "three"
window.M = require "./materials"
window.options = require "./options"
window.materials = M.materials
window.Material = M.Material

window.inputs = require "./inputs"
Mouse = inputs.Mouse
Keys = inputs.Keys
# window.barkdown = require "./barkdown"
$ = require "jquery"

dat = require "dat.gui"


# document.body.innerHTML = cards.map((c) -> "<img src='images/#{c.file}_a.png'/>")




options = window.options
width = window.innerWidth
height = window.innerHeight

T = window.T
V2 = T.Vector2
V3 = T.Vector3

default_corners = () ->
  s = 20
  e = options.screen.h - 20
  [
    new V2(s, e)
    new V2(e, e)
    new V2(e, s)
    new V2(s, s)
  ]
default_board = () ->
  columns : 4
  rows : 4
  corners : default_corners()
  midi : 
    device : options.midi_outputs[0]
    channel : 1
    start : 0

camera = null
renderer = null
render_target = null
cam_image = null
cursor = null
# cursor_color = new Float32Array([])

gui = null
options_gui = null

detectors = []

boards = []

window.dragged = null
drag_offset = new V2(0,0)

window.scene = new T.Scene()
window.sceneScreen = new T.Scene()

# corner_letters = options.corner_names.split("").map (n) -> new T.Geometry()

video = document.getElementById 'video'
video_texture = null

manager = new T.LoadingManager()
manager.onProgress = (url, items_loaded, total) ->
  $("#loading").html("[ #{items_loaded} / #{total} ]")
manager.onLoad = () ->
  init_help()
  init()
  # create_capture () -> init_midi init

file_loader = new T.FileLoader manager
tex_loader = new T.TextureLoader manager
font_loader = new T.FontLoader manager

window.atlas = null

load = () ->  
  materials.load file_loader
  window.atlas = tex_loader.load "images/atlas.#{Card.atlas_format}"

  # for c in cards
  #   c.large_tex = tex_loader.load "images/#{c.file}.#{Card.format}"

  # font_loader.load( './font/terminus.json', ( font ) ->
  #   font_options = 
  #     font: font,
  #     size: options.font_size,
  #     height: 5,
  #     curveSegments: 12,
  #     bevelEnabled: false,
  #   corner_letters = options.corner_names.split("").map (n) -> new T.TextGeometry( n, font_options)
  # )

error = (type) ->
  # $("#help-container").removeClass("closed")
  # $("#help").html barkdown.render(options.error_messages.header + options.error_messages[type])
  

stringify_vector = (v) ->
  JSON.stringify({x : v.x, y : v.y})
local_save = (x, name) ->
  window.localStorage.setItem name, JSON.stringify(if Array.isArray x then x.map((a) -> stringify_vector a) else x)
save_board = (index) ->
  local_save boards[index].save(), "board#{index}"

local_load = (name, x) ->
  l = window.localStorage.getItem name
  if l? 
    parsed = JSON.parse(l)
    if parsed?
      parsed
    else
      x
  else
    x
color_pick_target = null
start_color_pick = (target) ->
  color_pick_target = target
raycaster = new T.Raycaster()
intersects = []

top_card = (ls) ->
  cs = ls.filter((i) -> i.object.card?)
  if cs.length > 0
    cs.sort((a, b) -> b.object.position.z - a.object.position.z)
    cs[0].object
  else 
    null

swap = (arr, from, to) ->
  arr.splice(from, 1, arr.splice(to, 1, arr[from])[0])

update_raycaster = () ->
  m = new T.Vector3(Mouse.x, Mouse.y, 0)
  cursor.position.set m.x, m.y, 0
  raycaster.set m, new T.Vector3(0,0,-1)


events = 
  keydown : (k) ->
  mousedown : (e) ->
    if intersects.length > 0
      window.dragged = top_card intersects

      if window.dragged?
        i = cards.findIndex((c) -> c.file is window.dragged.card.file)
        d = cards.splice i, 1
        cards.push d[0]

        for c, i in cards
          p = c.mesh.position
          c.mesh.position.set p.x, p.y, (- (cards.length - i))

        window.dragged.card.flip()
        drag_offset = new V2(window.dragged.position.x - Mouse.x, window.dragged.position.y - (Mouse.y))
  mouseup : (e) ->
    if window.dragged?
      window.dragged = null

  mousemove : (e) ->
    update_raycaster()
    intersects = raycaster.intersectObjects sceneScreen.children
    
    c = top_card(intersects)
    $("#card-info").html(if c? then c.card.print() else "")
  
    ho = height - options.screen.h
    if window.dragged?
      window.dragged.position.set Mouse.x + drag_offset.x, Mouse.y  + drag_offset.y, 0
      cursor.position.set Mouse.x, Mouse.y, 0
    else
      cursor.position.set Mouse.x, Mouse.y, 0


window.grid_geom = (ps, sw, sh, w, h) ->
  pss = ps.slice(0, 3).concat(ps.slice(2, 4)).concat [ ps[0] ]
  g = new T.BufferGeometry()
  vertices = new Float32Array( [
    0,  0,  0
    sw,  0,  0
    sw,  sh,  0

    sw,  sh,  0
    0,  sh,  0
    0,  0,  0
  ] );  
  g.setAttribute( 'position', new T.BufferAttribute( vertices, 3 ) )

  uvs = new Float32Array(
    pss.reduce((a, v, i) ->
      a.concat [v.x / w, v.y / h]
    , [])
  )
  g.setAttribute( 'uv', new T.BufferAttribute( uvs, 2 ) )
  g

save_options = () ->
  local_save options, "options"

toggle_help = (show) ->
  # if not show?
    # show = $("#help-container").hasClass "closed"
  # $("#help-container").toggleClass "closed", not show
  local_save not show, "closed_help"
  save_options()

init_help = () ->
  options.closed_help = local_load("closed_help", options.closed_help)

  # options.help = barkdown.render(options.help) + 
  #     "<br>made by <a href='https://unlessgames.com/?t=d' target='_blank'>unless games</a><br>" + 
  #     "<br>click <a onclick='close_help()'>here</a> to minimize this!<br>" +
  #     "<br>have fun!<br>"

  # $("#help-container")
  #   .toggleClass "closed", options.closed_help
  #   .html $("<div>", html : options.help, class : "display", id: "help")
  # $("#help-button").click () -> toggle_help()


# init_gui = () ->
#   gui = new dat.GUI autoPlace : false

#   # $("#gui-container").html gui.domElement

#   options_gui = gui.addFolder "OPTIONS"

#   options_gui.add options, "resolution", options.resolutions
#     .onChange (v) =>
#       save_options()
#       window.location.reload()

#   # new Togglui(options_gui, options, "show_camera", 
#   #   ["disable rendering", "enable rendering"], true, 
#   #   () -> renderer.clear()
#   # )

#   r = 
#     reset_defaults : () ->
#       window.localStorage.clear()
#       local_save options.closed_help, "closed_help"
#       window.location.reload()

#   options_gui.add r, "reset_defaults"
#   options_gui.add options, "refresh_cycle", 1, 128
#     .step 1
#     .onChange (v) =>
#       save_options()

#   options_gui.add(options, "detector", options.detector_list).onChange((v) =>
#     save_options()
#     window.location.reload()
#   ).name "detector mode"

#   options_gui.open()
  

#   materials.init_gui gui
#   materials[options.detector].gui.show()

#   # gui.add(window, "add_new_grid").name("+ NEW GRID")

close_help = () ->
  toggle_help false
  local_save true, "closed_help"

bind_events = () ->
  window.addEventListener( 'resize', resize, false );
  Keys.init window
  Keys.listen(events, "keydown", "press", "all")

  Mouse.init $("canvas").get(0)
  Mouse.listen events, "mousedown", "press", window.inputs.MouseLeft
  Mouse.listen events, "mouseup", "release", window.inputs.MouseLeft
  $(window).mouseup events.mouseup
  Mouse.listen events, "mousemove", "move"

layout = (cs) ->
  cols = [1, 2, 3, 4, 5]
  col = 0
  margin = 0.2 * Card.scale
  ps = []
  for c, i in cs
    h = c.height * Card.scale * 2
    if col < cols.length
      offset = height - margin
      for ci in [0..cols[col] - 1]
        offset -= c.height * Card.scale * 0.5
        ps.push(
          x : margin + Card.scale * 0.5 + cols[col] * (Card.scale + margin)
          y : offset - Card.scale
        )
        offset -= margin
      col++
    else
      ps.push(
        x : margin + Card.scale * 0.5
        y : height - (Card.scale + margin) * 1
        flip : (i is (cs.length - 1))
      )


  ps.map((p) ->
    s = 0.1
    p.r = 0.5 * s - Math.random() * s
    p
  )

init_cards = () ->
  lay = layout(cards)

  for c, i in cards
    # s = Math.min(window.innerHeight, window.innerWidth) / 8
    s = Card.scale

    x = i % 8
    y = Math.floor(i / 8)
    w = Card.scale

    x = x * s + w * 0.5
    y = y * s  + w * 0.5



    x = i
    y = 0
    s = s * 0.5

    b = 100
    x = b + Math.random() * (width - b)
    y = b + Math.random() * (height - b)


    x = lay[i].x
    y = lay[i].y

    c.init(materials.saturator.material)

    c.mesh.position.set x, y, - (cards.length - i)

    console.log c
    if c.rots[0]
      c.mesh.rotateZ Math.PI * 0.5
    
    c.mesh.name = "card_" + c.id
    # scene.add m
    c.mesh.rotateZ lay[i].r
    sceneScreen.add c.mesh

    # if i == cards.length - 1
    #   console.log "flipping card"
    #   c.flip()



create_objects = () ->
  cam_image = new T.Mesh(
    new T.PlaneBufferGeometry options.screen.w, options.screen.h
    materials.screen.material
    # new T.MeshBasicMaterial( map: video_texture, side : T.DoubleSide )
  )
  cam_image.position.set options.screen.w * 0.5, height - options.screen.h * 0.5 , 0
  cam_image.visible = options.show_camera

  cursor = new T.Mesh new T.BoxGeometry(20, 20, 1), materials.line.material
  cursor.name = "cursor"
  cursor.position.set width * 0.5, height * 0.5

  uvs = [
    new V2 0, 0
    new V2 1, 0
    new V2 1, 1
    new V2 0, 1
  ]
  quad = new T.Mesh( grid_geom(uvs, 512, 512, 1, 1), materials.screen.material )
  quad.name = "magni quad"
  quad.position.set 0, 0, -10

  [cursor, quad]
  [cursor]
  # []

init_renderers = () ->
  render_target = new T.WebGLRenderTarget( 
    1024
    1024
    antialias : true
    minFilter: T.LinearFilter
    magFilter: T.LinearFilter
    format: T.RGBAFormat
    type: T.FloatType
  )

  renderer = new T.WebGLRenderer(
    antialias: true
    magFilter: T.LinearFilter
    minFilter: T.LinearFilter
  )
  renderer.setPixelRatio window.devicePixelRatio
  renderer.setSize window.innerWidth, window.innerHeight

  $("body").append renderer.domElement

load_options = () ->
  v = options.version

  saved_options = local_load "options", options

  for k of saved_options
    options[k] = saved_options[k]

  if options.version isnt v
    window.localStorage.clear()
    window.location.reload()

  rs = options.resolution.split "x"
  options.screen.w = parseInt(rs[0])
  options.screen.h = parseInt(rs[1])

  width = window.innerWidth
  height = window.innerHeight
  options.plane_width = width
  options.plane_height = height

window.magni = 
  camera : new T.OrthographicCamera -512, 512, 512, -512, -10, 1000
  scene : new T.Scene()

init = () ->
  load_options()

  materials.init()

  init_renderers()

  materials.saturator.update_uniform "tex0", atlas
  materials.screen.update_uniform "tex0", render_target.texture

  camera = new T.OrthographicCamera 0, width, height, 0, -100, 100
  # camera.position.z = -10

  magni.camera.position.z = -1

  # magni.scene.add Card.load(cards[0]).init_large(materials.grayscale.material).mesh

  for o in create_objects()
    sceneScreen.add o



  # scene.add cam_image

  # init_gui()
  # materials.hue_values.gui.show()
  # materials.hue_values.add_color_picker "hue1"
  
  init_cards()

  bind_events()
  
  $("#loading").remove()
  animate();

resize = () ->
  window.location.reload()
  # width = window.innerWidth
  # height = window.innerHeight
  # camera = new T.OrthographicCamera( 0, width, height, 0, -10, 1000 );
  # for b in boards
  #   b.generate_geoms()
  # renderer.setSize( width, height )

frames = 0
time = 0
last_time = 0
animate = () ->

  requestAnimationFrame animate

  now = new Date().getTime() * 0.001

  dt = now - last_time

  renderer.setRenderTarget render_target

  renderer.clear()
  renderer.render magni.scene, magni.camera

  nt = time + options.speed * dt
  time = nt - Math.floor(nt)

  materials.line.update_uniform "time", time

  # frames++
  # if frames % options.refresh_cycle is 0
  #   # boards[0].update_grid()
  #   # for b in boards
  #   #   b.update_grid()
  # else
  #   # boards[1].update_grid()

  # if options.show_camera
  renderer.setRenderTarget null
  renderer.render sceneScreen, camera

  update_raycaster()

  


  last_time = now


load()

# remove_grid = (i) ->
#   b = boards.splice i, 1
#   grid_gui.__folders["##{i}"] = undefined
#   boards.map((b, i) -> 
#     b.index = i
#     b.generate_geoms()
#   )
# add_new_grid = () ->
#   boards.push new Board options.grid_width, options.grid_height, material, local_load( "selection"), boards.length
#   for b in boards
#     sceneScreen.add b.selection
#     for o in b.objects
#       scene.add o
