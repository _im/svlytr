IMAGE = 0
TEXT = 1
T = require "three"
module.exports = 
  Card : 
    class Card
      # config
      @atlas_format : "png"
      @image_width : 512
      @format : "png"
      @scale : 200
      @size : 
        small : 400
        large : 400

      @from_filename : (n) ->
        indices = 
          id : 0
          date : 1
          group : 2

        n = n.split(".")[0]
        ns = n.split("_")

        Card.load(
          file : ns.slice(0,3).join("_")
          id : parseInt(ns[indices.id] - 1)
          date : parseInt(ns[indices.date])
          group : ns[indices.group]
        )
      @clamp_to_square = (x) ->
        sqs = 
          for i in [0..8]
            Math.pow(2, (4 + i))
        for s in sqs
          if x < s
            return s
        x
      @atlas_width : () -> Card.clamp_to_square(Card.size.small * 8)

      @load : (d) ->
        new Card(d.file, d.id, d.group, d.date, d.height, d.rots, d.text)

      constructor : (@file, @id, @group, @date, @height = 0, @rots = [false, false], @text) ->
      print : () -> "[#{@id + 1}] - #{@date} (#{@group})\n#{@text}"
      normal_height : (md) ->
        (md.height / md.width) / 2

      set_height : (md, side = 0) ->
        h = 
          if md.height > md.width
            @normal_height md
          else
            @rots[side] = true
            (md.width / md.height) / 2

        if h > @height
          @height = h
      set_size : (md, side = 0) ->
        @set_height md, side

      verts : (w) ->
        h = @height * w
        w *= 0.5
        [
          - w, - h, 0
          w, - h, 0
          w, h, 0
          w, h, 0
          - w, h, 0
          - w, - h, 0
        ]
      uvs : (s = 0, o = 0) ->
        p = o * (1 / (Card.size.small / 2))
        w = 0.5
        s *= 0.5
        h = @height
        [
          s + p, p
          s + w - p, p
          s + w - p, h - p
          s + w - p, h - p
          s + p, h - p
          s + p, p
        ]

      get_atlas_pos : (i) ->
        w = Card.atlas_width()
        r = Math.floor(Card.atlas_width() / Card.size.small)
        x = 0
        y = Card.size.small

        x = 
          if i % r isnt 0
            [0..(i % r) - 1].reduce(((a,v) -> a += Card.size.small), 0)
          else
            0
        y = (Math.floor(i / r) + 1) * Card.size.small

        p =
          x : x
          y : y
        p

      atlas_uvs : (s = 0, o = 0) ->
        r = (Card.atlas_width() / Card.size.small)
        w = 1 / r
        p = @get_atlas_pos(@id)
        x = p.x / Card.atlas_width()
        y = p.y / Card.atlas_width() - w
        @uvs(s, o).map((u, i) -> if i % 2 is 0 then x + (u / r) else y + (u / r))
        # @uvs(s, o).map((u, i) -> if i % 2 is 0 then (u / 8) + x * w else ((u / 8) + y * w))

      grid_geom : (vs, uv) ->
        g = new T.BufferGeometry()
        g.setAttribute( 'position', new T.BufferAttribute( new Float32Array(vs), 3 ) )
        g.setAttribute( 'uv', new T.BufferAttribute( new Float32Array(uv), 2 ) )
        g

      init : (mat) ->
        @geos = [
          @grid_geom(@verts(Card.scale), @atlas_uvs(0, 1))
          @grid_geom(@verts(Card.scale), @atlas_uvs(1, 1))
        ]
        @mesh = new T.Mesh(@geos[@current_side], mat)
        @mesh.card = @
        @
      init_large : (mat) ->
        @geos = [
          @grid_geom(@verts(Card.size.small), @uvs(0))
          @grid_geom(@verts(Card.size.small), @uvs(1))
        ]
        @mesh = new T.Mesh(@geos[@current_side], mat)
        @mesh.card = @
        @
      current_side : IMAGE

      flip : (side) ->
        @current_side = 
          if side? then side
          else 1 - @current_side
        @mesh.rotation.z = Math.PI * (if @rots[@current_side] then 0.5 else 0)
        @mesh.geometry = @geos[@current_side]



  Talon : 
    class Talon
      order : () ->
        @cards = @cards.sort((a, b) -> a.date - b.date)
      constructor : (@cards = []) ->
      add : (card) ->
        _card = @cards.find (c) -> c.file is card.file
        if not _card?
          @cards.push card

