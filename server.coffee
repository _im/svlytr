assert = require "assert"
fs = require "fs"
express = require "express"
jimp = require "jimp"

texer = require "./texer"
Card = require("./card").Card
Talon = require("./card").Talon


app = express()

process.env.PORT = 8000





raw = fs.readdirSync("./raw").filter((x) -> x isnt "texts.xlsx" and x isnt "szovegek.docx")

imfs = fs.readdirSync("./images")
for i in imfs 
  fs.unlinkSync "./images/#{i}"

talon = new Talon()

parse_texts = () ->
  console.log "\nreading raw/text.xlsx"
  XLSX = require('xlsx');
  sheet_name = "Sheet1"
  txts = XLSX.readFile('raw/texts.xlsx');
  for r of txts.Sheets[sheet_name]
    if r[0] is "A"
      i = parseInt(r.substring(1))
      t = txts.Sheets[sheet_name][r].v
      c = talon.cards.find((c) -> c.id is i - 1)
      if c?
        c.text = t
      console.log (if not c? then "\n!!  NO CARD WITH ID #{i}" else i), t

file_size = (f) ->
  fs.statSync(f).size / 1000000.0

init = () ->
  # Accept JSON as req.body
  bodyParser = require "body-parser"
  app.use(bodyParser.urlencoded({ extended: false }))
  app.use(bodyParser.json())

  # Configure browserify middleware to serve main.coffee as main.js
  browserify = require('browserify-middleware')
  browserify.settings
    transform: ['coffeeify']
    extensions: ['.coffee', '.litcoffee']
  app.use '/main.js', browserify(__dirname + '/main.coffee')

  # CORS - Allow pages from any domain to make requests to our API
  app.use (req, res, next) ->
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()

  # Log all requests for diagnostics
  app.use (req, res, next) ->
    console.log(req.method, req.path, req.body)
    next()
    
  # Serve Static files from public/
  app.use express.static('./')

  # Listen on App port


  console.log "removing temp files"
  for c in talon.cards
    fs.unlinkSync "./images/#{c.file}_a.#{Card.format}"

  parse_texts()

  fs.writeFileSync("./images/cards.json", JSON.stringify(talon.cards))
  
  console.log talon.cards
  console.log "\nwriting metadata to ./images/cards.json"

  atlas = file_size "./images/atlas.png"
  textures = 0
  for _c in talon.cards
    textures += file_size "./images/#{_c.file}.#{Card.format}"

  all = (textures + atlas).toFixed(2)
  textures = textures.toFixed(2)
  atlas = atlas.toFixed(2)
  jpg = file_size("./images/atlas.jpg").toFixed(2)

  console.log "\ngenerating atlas..."
  console.log "done."

  console.log "\ncards generated!\n"
  console.log "large textures #{textures} MB"
  console.log "atlas.png #{atlas} MB"
  console.log "atlas.jpg #{jpg} MB"
  console.log "------------------"
  console.log "total size #{all} MB\n"



  listener = app.listen(process.env.PORT, () -> console.log("http://localhost:#{process.env.PORT}"))



  connectio = (socket) ->
    
  io = require("socket.io")(listener)
  io.on("connection", connectio)

raw.map((filename) -> 
  talon.add Card.from_filename(filename)
)


texer.create_image_maps talon.cards, init

# console.log JSON.parse(fs.readFileSync("images/cards.json"))
# process.exit()




###
  generate small images

###